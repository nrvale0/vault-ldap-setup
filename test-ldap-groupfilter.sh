#!/bin/bash

if ! command -v ldapsearch > /dev/null 2>&1 ; then
    printf "\nProgram 'ldapsearch' was not found in PATH. Is it installed?\n\n"
    echo "PATH: ${PATH}"
    exit -1
fi

set -eux

ldapsearch \
    -h "${LDAP_SERVER}" \
    -p "${LDAP_SERVER_PORT}" \
    -b "${LDAP_BASE_DN}" \
    -D "${LDAP_BIND_DN}" \
    -w "${LDAP_BIND_PASSWORD}" \
    -s sub \
    "${LDAP_TEST_GROUP_FILTER}"
