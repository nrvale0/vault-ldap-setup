#!/bin/bash

[ -z "${DEBUG}" ] || set -x
set -ue

echo "Checking for running Vault..."
(set -x ; vault status)

echo "Enabling LDAP auth backend..."
set +e
if vault auth -methods | grep ldap 2>&1 > /dev/null ; then
    set -e; (set -x ; vault auth-disable ldap); sleep 5
fi
set -e; (set -x ; vault auth-enable ldap)

echo "Configuring LDAP auth backend..."
(set -x ; vault write auth/ldap/config \
		url="${LDAP_URL}" \
		userdn="${LDAP_USER_DN}" \
		groupdn="${LDAP_GROUP_DN}" \
		groupfilter="${LDAP_GROUP_FILTER}" \
		groupattr="${LDAP_GROUPATTR}" \
		insecure_tls=true \
		starttls=false \
		binddn="${LDAP_BIND_DN}" \
		bindpass="${LDAP_BIND_PASSWORD}")
